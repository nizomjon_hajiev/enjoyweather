class WeatherModel(object):
    def __init__(self, temp=None, hum=None, date=None):
        self.temp = temp
        self.hum = hum
        self.date = date

    @property
    def serialize(self):
        return {
            'temp': self.temp,
            'hum': self.hum,
            'date': self.date
        }
