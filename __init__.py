import gc
from functools import wraps
import hashlib
from flask import Flask, render_template, jsonify, request, url_for, Response
from flask import flash
from flask import json
from flask import redirect
from flask import session
from flask_sqlalchemy import SQLAlchemy
from flask_wtf import Form, validators
from sqlalchemy.orm import sessionmaker, scoped_session
from sqlalchemy import create_engine
from sqlalchemy import desc
from wtforms import TextField, PasswordField, BooleanField, StringField

app = Flask(__name__, static_url_path='')
app.config.from_pyfile('config.py')
app.secret_key = 's3cr3t'
db = SQLAlchemy(app)


class User(db.Model):
    __tablename__ = 'user'
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(50))
    email = db.Column(db.String(50))
    password = db.Column(db.String(50))

    def __init__(self, username, email, password):
        self.username = username
        self.email = email
        self.password = password

    @property
    def serialize(self):
        return {
            'id': self.id,
            'username': self.username,
            'email': self.email,
            'password': self.password
        }


class Humidity(db.Model):
    __tablename__ = 'sensor_humidity'
    id = db.Column(db.Integer, primary_key=True)
    hum = db.Column(db.Float)
    time_stamp = db.Column(db.String(50))
    sensor_id = db.Column(db.Integer)

    def __init__(self, hum, time_stamp, sensor_id):
        self.hum = hum
        self.time_stamp = time_stamp
        self.sensor_id = sensor_id

    @property
    def serialize(self):
        return {
            'id': self.id,
            'hum': self.hum,
            'time_stamp': self.time_stamp,
            'sensor_id': self.sensor_id
        }


class HeatingHistory(db.Model):
    __tablename__ = 'heating_history'
    id = db.Column(db.Integer, primary_key=True)
    sensor_id = db.Column(db.String(10))
    heating_start_time = db.Column(db.String(50))
    heating_end_time = db.Column(db.String(50))
    calculated_heating_time = db.Column(db.String(50))

    def __init__(self, sensor_id, heating_start_time, heating_end_time, calculated_heating_time):
        self.sensor_id = sensor_id
        self.heating_start_time = heating_start_time
        self.heating_end_time = heating_end_time
        self.calculated_heating_time = calculated_heating_time

    def get_heating_start_time(self):
        return self.heating_start_time

    def get_heating_end_time(self):
        return self.heating_end_time

    def get_calculated_heating_time(self):
        return self.calculated_heating_time

    @property
    def serialize(self):
        return {
            'sensor_id': self.sensor_id,
            'heating_start_time': self.heating_start_time,
            'heating_end_time': self.heating_end_time,
            'calculated_heating_time': self.calculated_heating_time
        }


class Temperature(db.Model):
    __tablename__ = 'sensor_temperature'
    id = db.Column(db.Integer, primary_key=True)
    temp = db.Column(db.Float)
    time_stamp = db.Column(db.String(50))
    sensor_id = db.Column(db.Integer)

    def __init__(self, temp, time_stamp, sensor_id):
        self.temp = temp
        self.time_stamp = time_stamp
        self.sensor_id = sensor_id

    @property
    def serialize(self):
        return {
            'id': self.id,
            'temp': self.temp,
            'time_stamp': self.time_stamp,
            'sensor_id': self.sensor_id
        }


class Weather(db.Model):
    __tablename__ = 'weather'
    id = db.Column(db.Integer, primary_key=True)
    temp = db.Column(db.String(20))
    hum = db.Column(db.String(50))
    date = db.Column(db.String(50))

    def __init__(self, temp, hum, date):
        self.temp = temp
        self.hum = hum
        self.date = date

    @property
    def serialize(self):
        return {
            'id': self.id,
            'temp': self.temp,
            'hum': self.hum,
            'date': self.date
        }


class WeatherModel(object):
    def __init__(self, temp=None, hum=None, date=None):
        self.temp = temp
        self.hum = hum
        self.date = date

    @property
    def x(self):
        print("getter of x called")
        return self.temp

    @property
    def x(self):
        print("getter of x called")
        return self.date

    @property
    def x(self):
        print("getter of x called")
        return self.hum

    @property
    def serialize(self):
        return {
            'temp': self.temp,
            'hum': self.hum,
            'date': self.date
        }


# E:\PycharmProjects\enjoyweather

# database url for ubuntu
# engine = create_engine('sqlite:////home/nizom/PycharmProjects/Test/app.sqlite')
# database url for windows (Ahadjon)
# engine = create_engine('sqlite:///E:\\PycharmProjects\\enjoyweather\\app.sqlite')
# database url for windows (Nizomjon)
engine = create_engine('sqlite:///E:\\Pycharm Projects\\enjoyweather\\app.sqlite')
# engine = create_engine('sqlite:////var/www/FlaskApp/FlaskApp/app.sqlite')
Session = scoped_session(sessionmaker(bind=engine))


class RegistrationForm(Form):
    username = StringField('Username')
    email = StringField('Email Address')
    password = PasswordField('New Password'
                             )

    confirm = PasswordField('Repeat Password')
    accept_tos = BooleanField('I accept the Terms of Service and Privacy Notice (updated Jan 22, 2015)',
                              )


@app.route('/', methods=['GET', 'POST'])
def login():
    error = ''
    rows_count = 0
    global username, password
    if request.method == "POST":
        attempted_username = request.form['username']
        attempted_password = request.form['password']
        connection = engine.connect()
        x = connection.execute("SELECT * FROM user WHERE username = ? ", attempted_username)
        for row in x:
            username = row['username']
            password = row['password']
            rows_count += 1
        if rows_count != 0 and str(attempted_username) == str(username) and str(attempted_password) == str(
                password):
            session['logged_in'] = True
            session['username'] = request.form['username']
            flash("You are now logged in")
            return redirect(url_for("realTime"))
        else:
            error = "Invalid credentials. Try Again."
        return render_template("test_login.html", error=error)
    heating_history_list = HeatingHistory.query.order_by(desc(HeatingHistory.heating_end_time)).limit(5).all()
    return render_template("real_time_demo.html", heating_history=heating_history_list)


@app.route('/register/', methods=["GET", "POST"])
def register_page():
    rows_count = 0
    try:
        form = RegistrationForm(request.form)
        if request.method == "POST":
            username = form.username.data
            email = form.email.data
            password = ((str(form.password.data)))
            connection = engine.connect()
            x = connection.execute("SELECT * FROM user WHERE username = ?",
                                   ((username)))
            for row in x:
                rows_count += 1
            if rows_count > 0:
                error = ("That username is already taken, please choose another")
                return render_template('register.html', form=form, error=error)
            else:
                user = User(username, email, password)
                db.session.add(user)
                db.session.commit()
                flash("Thanks for registering!")
                gc.collect()
                session['logged_in'] = True
                session['username'] = username
                return redirect(url_for('login'))

        return render_template("register.html", form=form)

    except Exception as e:
        return (str(e))


@app.route('/api/v1/login/', methods=['GET', 'POST'])
def login_api():
    global username, password
    rows_count = 0
    error = ''
    try:

        if request.method == "POST":
            json_dict = request.get_json()
            attempted_username = json_dict['username']
            attempted_password = json_dict['password']

            connection = engine.connect()

            x = connection.execute("SELECT * FROM user WHERE username = ? ", attempted_username)
            for row in x:
                username = row['username']
                password = row['password']
                rows_count += 1

            if rows_count != 0 and str(attempted_username) == str(username) and str(attempted_password) == str(
                    password):
                data = {
                    'username': username,
                    'status': 'success'
                }
                return Response(json.dumps(data), 200, mimetype='application/json')
            else:
                error = "Invalid credentials. Try Again."
                data = {
                    'status': 'error',
                    'error': error
                }
            return Response(json.dumps(data), 401, mimetype='application/json')
    except Exception as e:
        # flash(e)
        return e


@app.route("/api/v1/register/", methods=['GET', 'POST'])
def register_api():
    rows_count = 0
    if request.method == 'POST':
        json_dict = request.get_json()
        username = json_dict['username']
        email = json_dict['email']
        password = json_dict['password']
        connection = engine.connect()

        x = connection.execute("SELECT * FROM user WHERE username = ?",
                               ((username)))
        for row in x:
            rows_count += 1
        if rows_count > 0:
            data = {
                'status': 'error',
                'error': 'That username is already taken, please choose another'
            }
            return Response(json.dumps(data), 401, mimetype='application/json')

        else:
            user = User(username, email, password)
            db.session.add(user)
            db.session.commit()

            return Response(json.dumps(user.serialize), 201, mimetype='application/json')


@app.route('/real_time/', methods=['POST', 'GET'])
def realTime():
    global r
    if request.method == "POST":
        json_dict = request.get_json()
        a = json_dict['temp']
        b = json_dict['hum']
        c = json_dict['date']
        weather = Weather(a, b, c)
        db.session.add(weather)
        db.session.commit()
        data = {
            'temp': a,
            'hum': b,
            'time': c
        }
        return jsonify(data), 201
    elif request.method == "GET":
        heating_history_list = HeatingHistory.query.all()
        return render_template('real_time_demo.html', heating_history=heating_history_list)


def login_required(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        if 'logged_in' in session:
            return f(*args, **kwargs)
        else:
            flash("You need to login first")
            return redirect(url_for('login_page'))

    return wrap


@app.route("/logout/")
@login_required
def logout():
    session.clear()
    flash("You have been logged out!")
    gc.collect()
    return redirect(url_for('login'))



@app.route('/graph/')
def graph():
    weather_list = get_records_based_on_time('2017-02-10', '2017-02-30')

    time_adjusted_temperatures = []
    time_adjusted_humidities = []

    for record in weather_list:
        time_adjusted_temperatures.append(record.temp)
        time_adjusted_humidities.append(record.hum)

    list_count = len(weather_list)
    return render_template("graph.html", weather_list=weather_list, list_count=list_count)


@app.route('/api/v1/weather/history/', methods=['GET', 'POST'])
def history_weather_data():
    test = request.get_json()
    t1 = test[0]
    t2 = t1['start_time']
    weather_list = get_records_based_on_time(t1['start_time'], t1['finish_time'])

    time_adjusted_temperatures = []
    time_adjusted_humidities = []

    for record in weather_list:
        time_adjusted_temperatures.append(record.temp)
        time_adjusted_humidities.append(record.hum)

    return render_template("graph.html", temp=time_adjusted_temperatures, hum=time_adjusted_humidities)
    # return jsonify(json_list=[i.serialize for i in weather_list])


def get_records_based_on_time(from_time, to_time):
    weather_list = []
    connection = engine.connect()

    results = connection.execute("SELECT * FROM weather WHERE date BETWEEN ? AND ?",
                                 (from_time.format('YYYY-MM-DD'), to_time.format('YYYY-MM-DD')))
    for row in results:
        weather_list.append(WeatherModel(float(row['temp']), float(row['hum']), str(row['weather_date'])))
    connection.close()
    return weather_list


@app.route('/api/v1/heating_history/', methods=['GET', 'POST'])
def collect_heating_history():
    if request.method == "POST":
        json_dict = request.get_json()
        sensor_id = json_dict['sensor_id']
        heating_on_time = json_dict['heating_on_time']
        heating_off_time = json_dict['heating_off_time']
        calculated_heating_time = json_dict['calculated_heating_time']
        heating_history = HeatingHistory(sensor_id, heating_on_time, heating_off_time, calculated_heating_time)
        db.session.add(heating_history)
        db.session.commit()
    elif request.method == "GET":
        return jsonify(haeting_history=[i.serialize for i in HeatingHistory.query.all()])


@app.route('/api/v1/weather/', methods=['GET', 'POST'])
def collect_weather_data():
    if request.method == "POST":
        json_dict = request.get_json()
        type = json_dict['type']
        if type == 'humidity':
            hum = json_dict['hum']
            date = json_dict['date']
            sensor_id = json_dict['sensor_id']
            humidity = Humidity(hum, date, sensor_id)
            db.session.add(humidity)
            db.session.commit()
            data = {
                'sensor_id': sensor_id,
                'hum': hum,
                'time': date
            }
            return jsonify(data), 201
        elif type == 'temperature':
            temp = json_dict['temp']
            sensor_id = json_dict['sensor_id']
            date = json_dict['date']
            temperature = Temperature(temp, date, sensor_id)
            db.session.add(temperature)
            db.session.commit()
            data = {
                'sensor_id': sensor_id,
                'temp': temp,
                'time': date
            }
            return jsonify(data), 201
    else:
        result = (Humidity.query.all())

        return jsonify(json_list=[i.serialize for i in result])


@app.errorhandler(500)
def internal_server_error(e):
    return render_template('500.html', error=e)


if __name__ == '__main__':
    db.create_all()
    app.run(debug=True)
