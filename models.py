from sqlalchemy.testing import db


class Humidity(db.Model):
    __tablename__ = 'sensor_humidity'
    id = db.Column(db.Integer, primary_key=True)
    hum = db.Column(db.Float)
    time_stamp = db.Column(db.String(50))
    sensor_id = db.Column(db.Integer)

    def __init__(self, hum, time_stamp, sensor_id):
        self.hum = hum
        self.time_stamp = time_stamp
        self.sensor_id = sensor_id

    @property
    def serialize(self):
        return {
            'id': self.id,
            'hum': self.hum,
            'time_stamp': self.time_stamp,
            'sensor_id': self.sensor_id
        }

